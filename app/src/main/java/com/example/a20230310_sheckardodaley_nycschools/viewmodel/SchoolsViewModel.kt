package com.example.a20230310_sheckardodaley_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20230310_sheckardodaley_nycschools.models.NYCSchool
import com.example.a20230310_sheckardodaley_nycschools.repository.SchoolDataRepositoryImpl
import com.example.a20230310_sheckardodaley_nycschools.utilities.ResponseStates
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class SchoolsViewModel @Inject constructor(
    private val repo : SchoolDataRepositoryImpl
    ): ViewModel() {

    init {
        fetchAllNYCSchools()
    }

   private val listOfAllNYCSchools : MutableLiveData<ResponseStates> = MutableLiveData()
    val listOfAllNYCSchoolsLiveData : LiveData<ResponseStates> get() = listOfAllNYCSchools

    private val scoreDataForSchool : MutableLiveData<ResponseStates> = MutableLiveData()
    val scoreDataForSchoolLiveData  : LiveData<ResponseStates> get() = scoreDataForSchool

    var currSchool: NYCSchool? = null

    private fun fetchAllNYCSchools() {
        CoroutineScope(Dispatchers.IO).launch{
            repo.getNYCSchoolsFromApi().collect { response ->
                listOfAllNYCSchools.postValue(response)
            }
        }
    }

    fun fetchSATScoreForSchool(schoolCode : String) {
        CoroutineScope(Dispatchers.IO).launch{
            repo.getSATScoresFromApi(schoolCode).collect { response ->
                scoreDataForSchool.postValue(response)
            }
        }
    }

    fun setCurrentSchool(school: NYCSchool?) {
        currSchool = school
        scoreDataForSchool.value = ResponseStates.OnResponseLoading
    }
}
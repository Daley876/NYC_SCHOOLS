package com.example.a20230310_sheckardodaley_nycschools.repository

import com.example.a20230310_sheckardodaley_nycschools.network.SchoolApiService
import com.example.a20230310_sheckardodaley_nycschools.utilities.ResponseStates
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SchoolDataRepositoryImpl @Inject constructor(
    private val apiService : SchoolApiService
    ) : SchoolDataRepository
{


    override suspend fun getNYCSchoolsFromApi(): Flow<ResponseStates> {
        return flow {
            emit(ResponseStates.OnResponseLoading) //shows loading before api data is retrieved
            try {
                val response = apiService.getNYCSchools()
                if (response.isSuccessful) {
                    response.body()?.let { listOfSchools ->
                        emit(ResponseStates.OnResponseSuccess(listOfSchools)) //shows data if available
                    } ?: throw Exception("No Data Retrieved From Network")
                } else throw Exception("Error Encountered When Fetching Schools")

            } catch (e : Exception) {
                emit(ResponseStates.OnResponseError(e))
            }
        }
    }

    override suspend fun getSATScoresFromApi(code : String): Flow<ResponseStates> {
        return flow {
            try {
                val response = apiService.getSATScoreForSchool(code)
                if (response.isSuccessful) {
                    response.body()?.let { score ->
                        emit(ResponseStates.OnResponseSuccess(score)) //shows data if available
                    } ?: throw Exception("No Data Retrieved From Network")
                } else throw Exception("Error Encountered When Fetching School Score")

            } catch (e : Exception) {
                emit(ResponseStates.OnResponseError(e))
            }
        }
    }
}
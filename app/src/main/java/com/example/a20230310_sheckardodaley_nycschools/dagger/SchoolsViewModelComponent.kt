package com.example.a20230310_sheckardodaley_nycschools.dagger

import com.example.a20230310_sheckardodaley_nycschools.repository.SchoolDataRepositoryImpl
import com.example.a20230310_sheckardodaley_nycschools.viewmodel.SchoolsViewModel
import com.example.a20230310_sheckardodaley_nycschools.viewmodel.ViewModelFactory
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [SchoolsViewModelModule::class])
interface SchoolsViewModelComponent {

    fun getViewModel() : SchoolsViewModel

    fun getRepo() : SchoolDataRepositoryImpl

    fun getViewModelFactory() : ViewModelFactory
}
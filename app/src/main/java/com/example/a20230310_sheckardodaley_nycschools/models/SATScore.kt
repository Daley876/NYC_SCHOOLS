package com.example.a20230310_sheckardodaley_nycschools.models

import com.google.gson.annotations.SerializedName

data class SATScore(
    @SerializedName("dbn")
    val schoolCode: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("num_of_sat_test_takers")
    val totalTestTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val criticalReadingAvgScore: String,
    @SerializedName("sat_math_avg_score")
    val mathAvgScore: String,
    @SerializedName("sat_writing_avg_score")
    val writingAvgScore: String

)

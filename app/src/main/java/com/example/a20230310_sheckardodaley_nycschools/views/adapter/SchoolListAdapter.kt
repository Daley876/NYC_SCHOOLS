package com.example.a20230310_sheckardodaley_nycschools.views.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230310_sheckardodaley_nycschools.databinding.SchoolListItemLayoutBinding
import com.example.a20230310_sheckardodaley_nycschools.models.NYCSchool

class SchoolListAdapter (schoolList : MutableList<NYCSchool>, currentSchool : (NYCSchool) -> Unit
    ): RecyclerView.Adapter<SchoolListAdapter.SchoolViewHolder>() {

   private val listOfNYCSchools = schoolList
    private val viewScoreFunc = currentSchool

    private lateinit var binding: SchoolListItemLayoutBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
       val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        binding = SchoolListItemLayoutBinding.inflate(layoutInflater,parent,false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(listOfNYCSchools[position],viewScoreFunc)
    }

    override fun getItemCount(): Int {
       return listOfNYCSchools.size
    }

    fun updateSchoolList(schools: MutableList<NYCSchool>) {
        listOfNYCSchools.clear()
        listOfNYCSchools.addAll(schools)
        notifyItemRangeInserted(0,itemCount)
    }

    class SchoolViewHolder(binding: SchoolListItemLayoutBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        val name = binding.schoolName
        val addr = binding.schoolAddr
        val email = binding.schoolEmail
        val number = binding.schoolPhoneNumber
        val scoreBtn = binding.satBtn

            fun bind(school : NYCSchool,currentSchool : (NYCSchool) -> Unit) {

                if (school.address.contains('(')) {
                    val bracketIndex = school.address.indexOf('(')
                    val subStringAddr = school.address.subSequence(0,bracketIndex)
                    addr.text = subStringAddr
                } else addr.text = school.address

                name.text = school.schoolName
                email.text = school.email
                number.text = school.phoneNumber

                scoreBtn.setOnClickListener {
                    currentSchool(school)
                    }
            }
    }
}
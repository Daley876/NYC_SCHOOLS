package com.example.a20230310_sheckardodaley_nycschools.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230310_sheckardodaley_nycschools.R
import com.example.a20230310_sheckardodaley_nycschools.databinding.SchoolListFragmentLayoutBinding
import com.example.a20230310_sheckardodaley_nycschools.models.NYCSchool
import com.example.a20230310_sheckardodaley_nycschools.utilities.ResponseStates
import com.example.a20230310_sheckardodaley_nycschools.views.adapter.SchoolListAdapter


class SchoolListFragment : SchoolViewModelFragment() {

    private var schoolListBinding: SchoolListFragmentLayoutBinding? = null
    private val binding get() = schoolListBinding!!
    private lateinit var mAdapter : SchoolListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        schoolListBinding = SchoolListFragmentLayoutBinding.inflate(layoutInflater)
        initAdapter()
        initRecyclerView()
        initObserver()
        return binding.root

    }

    private fun initRecyclerView() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mAdapter
            setHasFixedSize(true)
        }
    }

    private fun initAdapter() {
        mAdapter = SchoolListAdapter(mutableListOf(),currentSchool = ::setCurrentSchool)
    }

    private fun initObserver() {
        viewModel.listOfAllNYCSchoolsLiveData.observe(viewLifecycleOwner) { state ->
            when (state) {
                is ResponseStates.OnResponseSuccess<*> -> {
                    binding.apply {
                        progressBar.visibility = View.GONE
                    }
                    val newList = state.response as MutableList<NYCSchool>
                    mAdapter.updateSchoolList(newList)
                }
                is ResponseStates.OnResponseError -> {
                    binding.apply {
                        progressBar.visibility = View.GONE
                    }
                }
                is ResponseStates.OnResponseLoading -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun setCurrentSchool (school: NYCSchool) {
        viewModel.setCurrentSchool(school)
        parentFragmentManager.beginTransaction()
            .replace(R.id.main_fragment_view, SchoolScoresFragment())
            .addToBackStack(null)
            .commit()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        schoolListBinding = null
    }
}
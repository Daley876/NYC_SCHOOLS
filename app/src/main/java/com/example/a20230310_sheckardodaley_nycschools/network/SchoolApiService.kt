package com.example.a20230310_sheckardodaley_nycschools.network

import com.example.a20230310_sheckardodaley_nycschools.models.NYCSchool
import com.example.a20230310_sheckardodaley_nycschools.models.SATScore
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApiService {

    @GET("s3k6-pzi2.json")
    suspend fun getNYCSchools() : Response<List<NYCSchool>>

    @GET("f9bf-2cp4.json") //this should return a list containing a single school and its scores
    suspend fun getSATScoreForSchool(
        @Query("dbn") dbn : String
    ) : Response<List<SATScore>>
}